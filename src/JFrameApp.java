import javax.swing.*;

public class JFrameApp extends JFrame {
    public JFrameApp()
    {
        setSize(250, 250);
        setLocationRelativeTo (null);

        final JLabel label = new JLabel("flag");

        setTitle("Calculator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add( new JPanelApp());
        setVisible(true);
    }
}

