import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class JPanelApp extends JPanel
{
    JTextField txt1 = null;
    JTextField txt2 = null;
    JTextField txtRes = null;
    JTextField txt4 = null;

    String num1str;
    String num2str;
    String operation;

    public JPanelApp()
    {

        try
        {
            setLayout(null);
            // Specifies the position of the element

            JLabel num1Label = new JLabel("Number1");
            num1Label.setBounds(5,10, 60,25);

            JLabel num2Label = new JLabel("Number2");
            num2Label.setBounds(5,50, 60,25);

            JLabel operationLabel = new JLabel("Operation");
            operationLabel.setBounds(5,95, 60,25);

            JLabel resultLabel = new JLabel("Result");
            resultLabel.setBounds(5,180, 60,25);

            TextField txt1 = new TextField();
            txt1.setBounds(65, 10, 165, 25);

            TextField txt2 = new TextField();
            txt2.setBounds(65, 50, 165, 25);

            TextField txtOper = new TextField();
            txtOper.setBounds(65, 95, 165, 25);

            TextField txtRes = new TextField();
            txtRes.setBounds(65, 180, 165, 25);

            JButton bRes = new JButton("CALCULATE");
            bRes.setBounds(10, 125, 220, 50);
            Font bigFont = new Font("serif", Font.PLAIN, 22);
            bRes.setFont(bigFont);

            add(txt1);
            add(txt2);
            add(txtOper);
            add(txtRes);
            add(bRes);
            add(num1Label);
            add(num2Label);
            add(operationLabel);
            add(resultLabel);

            bRes.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    num1str = txt1.getText();
                    num2str = txt2.getText();
                    operation = txtOper.getText();
                    int num1 = Integer.valueOf(num1str);
                    int num2 = Integer.valueOf(num2str);
                    String strOp = operation;
                    MethodCalc mc = new MethodCalc();
                    String strRes = String.valueOf(mc.calc(num1, strOp, num2 ));
                    txtRes.setText(strRes);
                }
            });
        }
        catch (ArithmeticException exception)
        {
            System.out.println("Can not divide by zero ");
        }
    }
}
