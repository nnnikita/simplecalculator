public class MethodCalc {
    public int calc ( int num1, String operation, int num2)
    {
        int res = 0;
        switch (operation)
        {
            case "+":
                res = num1 + num2;
                break;
            case "-":
                res = num1 - num2;
                break;
            case "*":
                res = num1 * num2;
                break;
            case "/":
                res = num1 / num2;
                break;
            default:
                res = 0;
                break;
        }
        return res;
    }
}
